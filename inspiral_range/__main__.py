import os
import sys
import h5py
import numpy as np
import argparse

from . import waveform
from . import inspiral_range

import logging

##################################################

description = """Calculate GW detector inspiral range from ASD/PSD

Strain spectra data should be two-column ascii, (freq, strain), and
may be given in amplitude or power (ASD or PSD).

Inspiral waveform parameters are specified as PARAM=VALUE pairs.  Mass
('m1'/'m2') parameters are assumed to be in solar masses (Msolar)
Default: m1 = m2 = 1.4 Msolar.

"""

def parse_params(clparams):
    params = {}
    for param in clparams:
        k,v = param.split('=')
        try:
            params[k] = float(v)
        except ValueError:
            params[k] = v
    return params

class FileAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, 'file', values)
        setattr(namespace, 'stype', self.dest)
        delattr(namespace, 'asd')
        delattr(namespace, 'psd')

class ParamsAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        params = parse_params(values)
        setattr(namespace, self.dest, params)

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description=description,
)
sg = parser.add_mutually_exclusive_group(required=True)
sg.add_argument('-a', '--asd', action=FileAction,
                help="assume noise spectrum is ASD")
sg.add_argument('-p', '--psd', action=FileAction,
                help="assume noise spectrum is PSD")
parser.add_argument('--params', dest='pprint', action='store_true',
                    help="print waveform parameters")
parser.add_argument('-c', '--calc',
                    help="output value of specific calculation only (from available inspiral_range functions)")
parser.add_argument('params', metavar='PARAM=VALUE', nargs='*', action=ParamsAction,
                    help="waveform parameters")

def main():
    args = parser.parse_args()

    params = waveform._get_waveform_params(**args.params)
    if args.pprint:
        for p in params:
            print('{} = {}'.format(p, params[p]))

    data = np.loadtxt(args.file)
    freq = data[1:,0]
    psd = data[1:,1]
    if args.stype == 'asd':
        psd **= 2

    if args.calc:
        func = eval('inspiral_range.{}'.format(args.calc))
        print('{} Mpc'.format(func(freq, psd, **params)))
        sys.exit()

    fmt = '{} = {} {}'

    print(fmt.format(
        'sensemon_range',
        inspiral_range.sensemon_range(freq, psd, params['m1'], params['m2']),
        'Mpc'))
    print(fmt.format(
        'sensemon_horizon',
        inspiral_range.sensemon_range(freq, psd, params['m1'], params['m2'], horizon=True),
        'Mpc'))

    ir = inspiral_range.cosmological_ranges(freq, psd, **params)

    for k,v in ir.items():
        print(fmt.format(k,*v))

##################################################

if __name__ == '__main__':
    main()
