"""Gravitational wave detector inspiral range calculations

This package provides functions for calculating various binary
"inspiral ranges" useful as figures of merit for gravitational wave
detectors characterized by a given strain noise spectral density.

See the following references for more information:

   https://dcc.ligo.org/LIGO-P1600071
   https://dcc.ligo.org/LIGO-T1500491
   https://dcc.ligo.org/LIGO-T1100338
   https://dcc.ligo.org/LIGO-T030276

"""

import os
import logging
logging.basicConfig(format='%(asctime)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=os.getenv('LOG_LEVEL', logging.WARNING))

from .waveform import gen_waveform
from .inspiral_range import *
