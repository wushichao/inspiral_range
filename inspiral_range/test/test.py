#!/usr/bin/env python

from __future__ import print_function
import os
import copy
import yaml
import numpy as np

from .. import cosmological_ranges

with open(os.path.join(os.path.dirname(__file__), 'compare.yaml')) as f:
    compare = yaml.load(f)

fmt = '  {:10} {:8.0f} {:8.0f} {:8.2f}'

psds = ['O2', 'O3', '2G', 'CE']

for psd in psds:

    psdfile = psd + '.txt'
    dtrm = compare[psdfile]

    print('{0}'.format(psdfile))
    path = os.path.join(os.path.dirname(__file__), psdfile)
    data = np.loadtxt(path)
    freq = data[:,0]
    psd = data[:,1]**2

    for mass,dtr in dtrm.items():

        approx = dtr['approximant']
        params = {}
        params['approximant'] = approx
        params['m1'] = mass
        params['m2'] = mass

        print('{0}/{0}, {1}'.format(mass, approx))
        
        print('  {:14} {:>8} {:>8} {:>8}'.format(
                '', 'IR', 'DT', '%d'))

        ir = cosmological_ranges(freq, psd, **params)

        for t in ir:
            val = ir[t][0]
            try:
                dtval = dtr[t]
            except KeyError:
                dtval = 0

            pd = ((dtval - val) / max(val, dtval)) * 100
        
            print('  {:14} {:8.0f} {:8.0f} {:8.2f}'.format(
                t, val, dtval, pd))

    print()
