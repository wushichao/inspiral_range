GW detector inspiral range calculation tools
============================================

This package provides python tools for calculating various binary
inspiral range measures useful as figures of merit for gravitational
wave detectors characterized a given strain noise spectral density.

It includes a command-line tool for calculating various inspiral
ranges from a supplied file of detector noise spectral density (either
ASD or PSD).

It depends on the 'lal' and 'lalsimulation' python packages, as well
as scipy.

See the following references for more information:

* https://dcc.ligo.org/LIGO-P1600071 (https://arxiv.org/abs/1709.08079)
* https://dcc.ligo.org/LIGO-T1500491
* https://dcc.ligo.org/LIGO-T1100338
* https://dcc.ligo.org/LIGO-T030276

Authors:

* Jolien Creighton <jolien.creighton@ligo.org>
* Jameson Rollins <jameson.rollins@ligo.org>


Tests
=====

The packages includes tests that compare the range calculations from
this package for various detector ASDs against those calculated from a
different implementation of these functions from arXiv:1709.08079:

    >>> python -m inspiral_range.test.test
